ShoppingCardUser.controller("template1Ctrl",function($scope,categoryServices,SubcategoryServices,productServices, $q){

    var getCategoryName = function () {
        return categoryServices.getAllcategoryDetails().then(function (res) {
            return res.data;
        });
    };

    var getSubcategoryName = function () {
         return SubcategoryServices.getAllsubcategoryDetails().then(function (res) {
             return res.data;
         });
    };

    var getProductname = function () {
          return productServices.getProductDetailsByName().then(function (res) {
              return res.data;
          });
    };

    $scope.initFun = function(){
        $q.all([
            getCategoryName(),
            getSubcategoryName(),
            getProductname()
        ]).then(function(values){
            var categories = values[0];
            var subcategories = values[1];
            var products = values[2];
            var myCategories = [];
            categories.forEach(function(category){
                category.subcategories = [];
                subcategories.forEach(function(subcategory){
                    if(category.categoryId == subcategory.categoryId){
                        subcategory.products = [];
                        products.forEach(function(product){
                            if(product.subCategoryid == subcategory.subCategoryid){
                                subcategory.products.push(product);
                            }
                        });
                        category.subcategories.push(subcategory);
                    }
                });
                myCategories.push(category);
            });
            $scope.myCategories = myCategories;
        });
    };

    $scope.callingProductBySubCat=function(subCatId)
    {
    productServices.getProductDetailsBySubCategoryId(subCatId).then(function(result){
    $scope.subcatProduct=result.data

    console.log(result.data);
    })
    }

    $scope.callingProductBySubCat=function(ProductName)
    {
    $scope.productList=[];
    productServices.getProductDetailsBySubCategoryId(ProductName).then(function(result)
    {
    $scope.subcatProductdetails=result.data
    console.log(result.data);
    for(i=0;i<$scope.subcatProductdetails.length;i++)
    {
    console.log($scope.subcatProductdetails[i])
    for (var key in $scope.subcatProductdetails[i])
    {
    if(key=="SubCategorieslist")
    {
    console.log($scope.subcatProductdetails[i][key]);
    var s=$scope.subcatProductdetails[i][key];
    for(j=0;j<s.length;j++)
    {
    $scope.productList.push(s[j].productName);
    }
    }
    }
    }
    console.log($scope.productList);
    })
    }


function init()
{
$scope.gettingProductBySubCat();
}

$scope.initFun();


    /*$scope.gettingProductBySubCat();

    $scope.initFun();*/
});










