ShoppingCardUser.controller("IndexCtrl",function($scope,categoryServices,SubcategoryServices,productServices,$q){

    var getCategoryName = function () {
        return categoryServices.getAllcategoryDetails().then(function (res) {
            return res.data;
        });
    };

    var getSubcategoryName = function () {
         return SubcategoryServices.getAllsubcategoryDetails().then(function (res) {
             return res.data;
         });
    };

    var getProductname = function () {
          return productServices.getProductDetailsByName().then(function (res) {
              return res.data;
          });
    };

    $scope.initFun = function(){
        $q.all([
            getCategoryName(),
            getSubcategoryName(),
            getProductname()
        ]).then(function(values){
            var categories = values[0];
            var subcategories = values[1];
            var products = values[2];
            var myCategories = [];
            categories.forEach(function(category){
                category.subcategories = [];
                subcategories.forEach(function(subcategory){
                    if(category.categoryId == subcategory.categoryId){
                        subcategory.products = [];
                        products.forEach(function(product){
                            if(product.subCategoryid == subcategory.subCategoryid){
                                subcategory.products.push(product);
                            }
                        });
                        category.subcategories.push(subcategory);
                    }
                });
                myCategories.push(category);
            });
            $scope.myCategories = myCategories;
        });
    };



    /*$scope.getdetailsforskill=function()
        {
        //console.log($scope.employeeJsonConfig);
        for(i=0;i<$scope.employeeJsonConfig.length;i++)
        {
        //employee
        if($scope.employeeJsonConfig[i].realName==="Name")
        {
        var j=i;
        //console.log(j);
        var employeeobj=[];
        //var employeearrayobj={};
        for(x=i;x<$scope.employeeJsonConfig.length;x++)
        {
        //console.log($scope.employeeJsonConfig[x]);
        employeeobj.push($scope.employeeJsonConfig[x]);
        $scope.employeeobject=employeeobj;
        //console.log("skill")
        //console.log(skillarrayobj);


        if($scope.employeeJsonConfig[x].realName==="Supervisor")
        {
        break;
        }
        }*/



 $scope.callingProductBySubCat=function(subCatId)
    {
    console.log(subCatId);
    productServices.getProductDetailsBySubCategoryId(subCatId).then(function(result){
    $scope.subcatProduct=result.data
productServices.saveData($scope.subcatProduct);
    console.log(result.data);
    })
    }

    $scope.initFun();






});










