shoppingAdminapp.factory('productServices',function($http){
var baseUrl='https://localhost:2000'
var postProductdetails = function(productdata)
{
console.log(productdata);
return $http.post('/product',productdata);
}
var getAllproductDetails = function()
{
return $http.get(baseUrl+'/allProduct');
}
var editProcuctbyMongoId = function(editobj)
{
console.log(editobj);
return $http.post('/editproductBymongoId',editobj);
}
var deleteProcuctbyMongoId= function(mongoid)
{
return $http.delete('/productBymongoId/'+mongoid);
}

var getNameForProduct=function()
{
return $http.get('/productName');
}

var setNameForProduct=function(getName)
{
if(getName){
console.log(getName)
return getName;
}
}

var getProductDetailsByName=function(productName)
{
return $http.get('/productByName/'+productName);
}

var getEditProductDetailsByMongoId=function(mongoid)
{
return $http.get('/productByMongoId/'+mongoid);
}
var getProductByCategorySubCategory=function()
{
return $http.get('/productListByCategoryAndSubCategory')
}

var getAllProductID=function()
{
return $http.get('/productId');
}



return {
postProductdetails:postProductdetails,
getAllproductDetails:getAllproductDetails,
editProcuctbyMongoId:editProcuctbyMongoId,
deleteProcuctbyMongoId:deleteProcuctbyMongoId,
getNameForProduct:getNameForProduct,
setNameForProduct:setNameForProduct,
getProductDetailsByName:getProductDetailsByName,
getEditProductDetailsByMongoId:getEditProductDetailsByMongoId,
getProductByCategorySubCategory:getProductByCategorySubCategory,
getAllProductID:getAllProductID
}
})