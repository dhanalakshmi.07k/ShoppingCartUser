var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ShoppingCompanySettingSchema = new mongoose.Schema(
  {
     companyName:String,
     companySlogan:String,
    companyCity:String,
    companyCountry:String,
    companyState:String,
    companyPINCode:String,
    companyEmail:String,
    companyWebSite:String,
    companyPhone:Number,
    companyAddress:String,
    companyContactPerson:String,
    companyLinkedInId:String,
    companyFBId:String,
    companyTwiterId:String,
    companyInstagramId:String,
    companyLogo:String
},{collection:"companySetting"});
  mongoose.model('companySetting',ShoppingCompanySettingSchema);